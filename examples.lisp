;;; -*- Mode: LISP; Syntax: COMMON-LISP; Base: 10 -*-
;;; arch-tag: 7debdb50-4feb-4e57-b6fd-a1d1eaf6ceab

;;; Copyright (c) 2006, Kilian Sprotte. All rights reserved.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:

;;;   * Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.

;;;   * Redistributions in binary form must reproduce the above
;;;     copyright notice, this list of conditions and the following
;;;     disclaimer in the documentation and/or other materials
;;;     provided with the distribution.

;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :cl-user)

;; TODO maybe use gecol:intvar-assigned
(defun cartesian-product ()
  (let ((s (gecol:make-gecolspace :intnum 3 :intmin 1 :intmax 3)))
    (gecol:with-var-arg-array ((loop for i below 3 collect (gecol:gecolspace-getint-int s i))
                               varargs)
      (gecol:branch-intvarargs-bvarsel-bvalsel s varargs :bvar-none :bval-min))
    (let ((e (gecol:make-dfs-space-int-int-stop s)))
      (loop
         for sol = (gecol:dfs-next e)
         until (cffi:null-pointer-p sol)
         do (format t "~a, ~a, ~a~%"
                    (gecol:intvar-val (gecol:gecolspace-getint-int sol 0))
                    (gecol:intvar-val (gecol:gecolspace-getint-int sol 1))
                    (gecol:intvar-val (gecol:gecolspace-getint-int sol 2)))
         do (gecol:delete-gecolspace sol))
      (gecol:delete-dfs e)
      (gecol:delete-gecolspace s))))

(defun distinct ()
  (let ((s (gecol:make-gecolspace :intnum 3 :intmin 1 :intmax 3)))
    (gecol:with-var-arg-array ((loop for i below 3 collect (gecol:gecolspace-getint-int s i))
                               varargs)
      (gecol:distinct-intvarargs-intconlevel s varargs :icl-def)
      (gecol:branch-intvarargs-bvarsel-bvalsel s varargs :bvar-none :bval-min))
    (let ((e (gecol:make-dfs-space-int-int-stop s)))
      (loop
         for sol = (gecol:dfs-next e)
         until (cffi:null-pointer-p sol)
         do (format t "~a, ~a, ~a~%"
                    (gecol:intvar-val (gecol:gecolspace-getint-int sol 0))
                    (gecol:intvar-val (gecol:gecolspace-getint-int sol 1))
                    (gecol:intvar-val (gecol:gecolspace-getint-int sol 2)))
         do (gecol:delete-gecolspace sol))
      (gecol:delete-dfs e)
      (gecol:delete-gecolspace s))))

(defun sorted ()
  (let ((s (gecol:make-gecolspace :intnum 3 :intmin 1 :intmax 3)))
    (gecol:rel-intvar-intreltype-intvar-intconlevel s
                                                    (gecol:gecolspace-getint-int s 0)
                                                    :irt-<
                                                    (gecol:gecolspace-getint-int s 1)
                                                    :icl-def)
    (gecol:rel-intvar-intreltype-intvar-intconlevel s
                                                    (gecol:gecolspace-getint-int s 1)
                                                    :irt-<
                                                    (gecol:gecolspace-getint-int s 2)
                                                    :icl-def)

    (gecol:with-var-arg-array ((loop for i below 3 collect (gecol:gecolspace-getint-int s i))
                               varargs)
      (gecol:branch-intvarargs-bvarsel-bvalsel s varargs :bvar-none :bval-min))
    (let ((e (gecol:make-dfs-space-int-int-stop s)))
      (loop
         for sol = (gecol:dfs-next e)
         until (cffi:null-pointer-p sol)
         do (format t "~a, ~a, ~a~%"
                    (gecol:intvar-val (gecol:gecolspace-getint-int sol 0))
                    (gecol:intvar-val (gecol:gecolspace-getint-int sol 1))
                    (gecol:intvar-val (gecol:gecolspace-getint-int sol 2)))
         do (gecol:delete-gecolspace sol))
      (gecol:delete-dfs e)
      (gecol:delete-gecolspace s))))

(defun cartesian-product-distr-max ()
  "Like cartesian-product, but using a different branching."
  (let ((s (gecol:make-gecolspace :intnum 3 :intmin 1 :intmax 3)))
    (gecol:with-var-arg-array ((loop for i below 3 collect (gecol:gecolspace-getint-int s i))
                               varargs)
      ;; branching
      (gecol:branch-intvarargs-bvarsel-bvalsel s varargs :bvar-none :bval-max))
    (let ((e (gecol:make-dfs-space-int-int-stop s)))
      (loop
         for sol = (gecol:dfs-next e)
         until (cffi:null-pointer-p sol)
         do (format t "~a, ~a, ~a~%"
                    (gecol:intvar-val (gecol:gecolspace-getint-int sol 0))
                    (gecol:intvar-val (gecol:gecolspace-getint-int sol 1))
                    (gecol:intvar-val (gecol:gecolspace-getint-int sol 2)))
         do (gecol:delete-gecolspace sol))
      (gecol:delete-dfs e)
      (gecol:delete-gecolspace s))))

(defun distinct-minimal-third ()
  "Like distinct, but minimizing the third variable."
  (let ((s (gecol:make-gecolspace :intnum 3 :intmin 1 :intmax 3
                                  :bab-intvar-ind 2 :bab-intreltype :irt-<)))
    (gecol:with-var-arg-array ((loop for i below 3 collect (gecol:gecolspace-getint-int s i))
                               varargs)
      (gecol:distinct-intvarargs-intconlevel s varargs :icl-def)
      (gecol:branch-intvarargs-bvarsel-bvalsel s varargs :bvar-none :bval-min))
    (let ((e (gecol:make-bab-space-int-int-stop s)))
      (loop
         for sol = (gecol:bab-next e)
         until (cffi:null-pointer-p sol)
         do (format t "~a, ~a, ~a~%"
                    (gecol:intvar-val (gecol:gecolspace-getint-int sol 0))
                    (gecol:intvar-val (gecol:gecolspace-getint-int sol 1))
                    (gecol:intvar-val (gecol:gecolspace-getint-int sol 2)))
         do (gecol:delete-gecolspace sol))
      (gecol:delete-bab e)
      (gecol:delete-gecolspace s))))
