;;;; Silly emacs, this is -*- Lisp -*-
;;;; arch-tag: 3AEA583B-0A11-1454-E9321DE74C68

;;; Copyright (c) 2006, Kilian Sprotte. All rights reserved.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:

;;;   * Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.

;;;   * Redistributions in binary form must reproduce the above
;;;     copyright notice, this list of conditions and the following
;;;     disclaimer in the documentation and/or other materials
;;;     provided with the distribution.

;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(defpackage :gecol-system (:use :asdf :cl))
(in-package :gecol-system)

;;;; C-SOURCE FILE HANDLING

(defvar *gcc* "/usr/bin/g++")
(defvar *gcc-options* '(#-(or :darwin :macosx) "-shared"
                        #+(or :darwin :macosx) "-dynamiclib"
                        "-fPIC"
                        "-lgecodesearch"
                        "-lgecodekernel"
                        "-lgecodeint"
                        "-lgecodeset"
                        "-lgecodeminimodel"
                        "-lstdc++"
"-lgecodesupport"



))

(defclass gecode-source-file (source-file)
  ())

(defmethod source-file-type ((c gecode-source-file) s)
  (declare (ignore s))
  "cpp")

(defmethod output-files ((o compile-op) (c gecode-source-file))
  (list (make-pathname :name (component-name c)
                       :type "so"
                       :defaults (component-pathname c))))

(defmethod perform ((o load-op) (c gecode-source-file))
  (let ((loader (intern (string '#:load-foreign-library) :cffi))) ; Allegro's Modern mode
    (dolist (file (asdf::input-files o c))
      (funcall loader file))))

(defmethod perform ((o compile-op) (c gecode-source-file))
  (unless (zerop (run-shell-command "~A ~A ~{~A ~}-o ~A"
                                    *gcc*
                                    (namestring (component-pathname c))
                                    *gcc-options*
                                    (namestring (car (output-files o c)))))
    (error 'operation-error :component c :operation o)))


(defsystem :gecol
  :name "gecol"
  :description "gecol"
  :author "paul"
  :version "0.1"
  :serial t
  :components
  ((:file "package")
   (:gecode-source-file "glue")
   (:file "ffi")
   (:file "gec"))
  :depends-on
  (:iterate :cffi))