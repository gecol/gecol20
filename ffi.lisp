;;; -*- Mode: LISP; Syntax: COMMON-LISP; Package: GECOL; Base: 10 -*-
;;; arch-tag: 09C25807-504D-487A-B9D3-275276CF17B1

;;; Copyright (c) 2006, Kilian Sprotte. All rights reserved.
;;; Mauricio Toro-Bermudez 2008. Extending gecol to work with gecode 2.0.x

;;; Thanks to Christian Schulte and Guido Tack (Gecode developers) and 
;;; Gustavo Gutierrez (GeOz developer) for their help.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:

;;;   * Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.

;;;   * Redistributions in binary form must reproduce the above
;;;     copyright notice, this list of conditions and the following
;;;     disclaimer in the documentation and/or other materials
;;;     provided with the distribution.

;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package :gecol)

(defmacro defcfun* (name &rest args)
  `(progn
     (eval-when (:compile-toplevel :load-toplevel :execute)
       (export ',(if (listp name) (second name) name)))
     (defcfun ,name ,@args)))

(defcenum space-status
  :ss-failed
  :ss-solved
  :ss-branch)

(defcenum int-rel-type
  :irt-=                                ; =
  :irt-/=                               ; /=
  :irt-<=                               ; <=
  :irt-<                                ; <
  :irt->=                               ; >=
  :irt->                                ; >
  )

(defcenum int-con-level
  :icl-val                              ; Value consistency (naive)
  :icl-bnd                              ; Bounds consistency.
  :icl-dom                              ; Domain consistency.
  :icl-def                  ;The default consistency for a constraint.
  )


(defcenum set-rel-type
  :srt-eq                               ; equality
  :srt-nq                               ; disequality
  :srt-sub                              ; subset
  :srt-sup                              ; superset
  :srt-disj                             ; disjoint
  :srt-cmpl                             ; complement
  )


(defcenum set-op-type
  :sot-union                            ; union
  :sot-dunion                           ; disjoint union
  :sot-inter                            ; intersection
  :sot-minus                            ; difference
  )

(defcenum bvar-sel
  :bvar-none
  :bvar-min-min
  :bvar-min-max
  :bvar-max-min
  :bvar-max-max
  :bvar-size-min
  :bvar-size-max
  :bvar-degree-min
  :bvar-degree-max
  :bvar-regret-min-min
  :bvar-regret-min-max
  :bvar-regret-max-min
  :bvar-regret-max-max
  )

(defcenum bval-sel
  :bval-min
  :bval-med
  :bval-max
  :bval-split-min
  :bval-split-max
  )

; new -- SetVarBranch
(defcenum setvar-sel
  :set-var-none
  :set-var-min-card
  :set-var-max-card
  :set-var-min-unk
  :set-var-max-unk
)

; new - SetValBranch
(defcenum setval-sel
  :set-val-min
  :set-val-max
)

;; REG (Regular expressions)
(defcfun* ("make_REG_void" make-REG-void) :pointer
  
)

(defcfun* ("make_REG_int" make-REG-int) :pointer
   (a :int)
)

;(defcfun* ("requal" r=) :pointer
 ; (a :pointer)
  ;(b :pointer))

(defcfun* ("rplus" r+) :pointer
  (a :pointer)
  (b :pointer))

(defcfun* ("ror" ror) :pointer
  (a :pointer)
  (b :pointer))

(defcfun* ("rklene" r*) :pointer
  (a :pointer)
)

(defcfun* ("extensional_IntVarArgs_DFA_intcontlevel" extensional-IntVarArgs-DFA-intcontlevel) :void
 (home :pointer)
  (x :pointer)
  (d :pointer)
  (icl int-con-level)
)


;; IntSet
(defcfun* ("make_IntSet_int_int" make-intset-int-int) :pointer
  (n :int)
  (m :int))

(defcfun* ("make_IntSet_intarray_int" make-intset-intarray-int) :pointer
  (r :pointer)
  (n :int))

(defcfun* ("make_IntSet_intarray2_int" make-intset-intarray2-int) :pointer
  (r :pointer)
  (n :int))

(defcfun* ("IntSet_size" intset-size) :int
  (intset :pointer))

(defcfun* ("IntSet_min_int" intset-min-int) :int
  (intset :pointer)
  (i :int))

(defcfun* ("IntSet_max_int" intset-max-int) :int
  (intset :pointer)
  (i :int))

(defcfun* ("IntSet_width_int" intset-width-int) :unsigned-int
  (intset :pointer)
  (i :int))

(defcfun* ("IntSet_min" intset-min) :int
  (intset :pointer))

(defcfun* ("IntSet_max" intset-max) :int
  (intset :pointer))

(defcfun* ("delete_IntSet" delete-intset) :void
  (intset :pointer))

;; IntVar
(defcfun* ("make_IntVar_Space_int_int" make-intvar-space-int-int) :pointer
  (space :pointer)
  (min :int)
  (max :int))
(defcfun* ("IntVar_min" intvar-min) :int
  (intvar :pointer))
(defcfun* ("IntVar_max" intvar-max) :int
  (intvar :pointer))
(defcfun* ("IntVar_med" intvar-med) :int
  (intvar :pointer))
(defcfun* ("IntVar_val" intvar-val) :int
  (intvar :pointer))
(defcfun* ("IntVar_size" intvar-size) :unsigned-int
  (intvar :pointer))
(defcfun* ("IntVar_width" intvar-width) :unsigned-int
  (intvar :pointer))
(defcfun* ("IntVar_degree" intvar-degree) :unsigned-int
  (intvar :pointer))
(defcfun* ("IntVar_assigned" intvar-assigned) :boolean
  (intvar :pointer))
(defcfun* ("delete_IntVar" delete-intvar) :void
  (intvar :pointer))

;; Space
(defcfun ("make_GecolSpace" %make-gecolspace) :pointer
  (intnum :int)
  (intmin :int)
  (intmax :int)
  (boolnum :int)
  (setnum :int)
  (bab-intvar-ind :int)
  (bab-intreltype int-rel-type))

(defcfun* ("delete_GecolSpace" delete-gecolspace) :void
  (gecolspace :pointer))

(defcfun* ("GecolSpace_getInt_int" gecolspace-getint-int) :pointer
  (space :pointer)
  (ind :int))

(defcfun* ("GecolSpace_getBool_int" gecolspace-getbool-int) :pointer
  (space :pointer)
  (ind :int))

(defcfun* ("GecolSpace_getSet_int" gecolspace-getset-int) :pointer
  "Access SetVar at IND in SPACE."
  (space :pointer)
  (ind :int))

(defcfun* ("GecolSpace_putSet_int_SetVar" gecolspace-putset-int-setvar) :void
  "Place the SetVar SET into SPACE.
After SPACE has been constructed with only the number of
SetVars given, this needs to be done for each one.

This allows for different initializations of each
SetVar by using GEC-FS-MAKE-*."
  (space :pointer)
  (ind :int)
  (set :pointer))

(defcfun* ("GecolSpace_status" gecolspace-status) space-status
  (space :pointer))

(defcfun* ("GecolSpace_description" gecolspace-description) :pointer
  (space :pointer))

(defcfun* ("GecolSpace_clone" gecolspace-clone) :pointer
  (space :pointer)
  (share :boolean))

(defcfun* ("GecolSpace_commit" gecolspace-commit) :void
  (space :pointer)
  (d :pointer)
  (a :unsigned-int))

(defcfun* ("GecolSpace_fail" gecolspace-fail) :void
  (space :pointer))

(defcfun* ("GecolSpace_failed" gecolspace-failed) :boolean
  (space :pointer))

(defcfun* ("GecolSpace_propagators" gecolspace-propagators) :unsigned-int
  (space :pointer))

(defcfun* ("GecolSpace_branchings" gecolspace-branchings) :unsigned-int
  (space :pointer))

;;; DFS
(defcfun ("make_DFS_Space_int_int_Stop" %make-dfs-space-int-int-stop) :pointer
  (space :pointer)
  (c-d :int)
  (a-d :int)
  (st :pointer))

(defcfun* ("delete_DFS" delete-dfs) :void
  (dfs :pointer))

(defcfun* ("DFS_next" dfs-next) :pointer
  (dfs :pointer))

;;; BAB
(defcfun ("make_BAB_Space_int_int_Stop" %make-bab-space-int-int-stop) :pointer
  (space :pointer)
  (c-d :int)
  (a-d :int)
  (st :pointer))

(defcfun* ("delete_BAB" delete-bab) :void
  (bab :pointer))

(defcfun* ("BAB_next" bab-next) :pointer
  (bab :pointer))

;;;;;;;;;

(defcfun*
    ("branch_intvarargs_bvarsel_bvalsel" branch-intvarargs-bvarsel-bvalsel) :void
  (home :pointer) (x :pointer) (vars bvar-sel) (vals bval-sel))

;new branching functions
(defcfun*
    ("branch_setvarargs_setvarbranch_setvalbranch" branch-setvarargs-setvarbranch-setvalbranch) :void
  (home :pointer) (x :pointer) (vars setvar-sel) (vals setval-sel))

(defcfun*
    ("branch_setvarbranch_setvalbranch" branch-setvarbranch-setvalbranch) :void
  (home :pointer) (vars setvar-sel) (vals setval-sel))

(defcfun*
    ("branch_bvarsel_bvalsel" branch-bvarsel-bvalsel) :void
  (home :pointer) (vars bvar-sel) (vals bval-sel))



(defcfun*
    ("linear_intvarargs_intreltype_int_intconlevel"
     linear-intvarargs-intreltype-int-intconlevel)
    :void (home :pointer) (x :pointer) (r int-rel-type) (c :int)
    (icl int-con-level))
(defcfun*
    ("linear_intvarargs_intreltype_intvar_intconlevel"
     linear-intvarargs-intreltype-intvar-intconlevel)
    :void (home :pointer) (x :pointer) (r int-rel-type) (y :pointer)
    (icl int-con-level))
(defcfun*
    ("linear_intvarargs_intreltype_int_boolvar_intconlevel"
     linear-intvarargs-intreltype-int-boolvar-intconlevel)
    :void (home :pointer) (x :pointer) (r int-rel-type) (c :int) (b :pointer)
    (icl int-con-level))
(defcfun*
    ("linear_intvarargs_intreltype_intvar_boolvar_intconlevel"
     linear-intvarargs-intreltype-intvar-boolvar-intconlevel)
    :void (home :pointer) (x :pointer) (r int-rel-type) (y :pointer) (b :pointer)
    (icl int-con-level))
(defcfun*
    ("linear_intargs_intvarargs_intreltype_int_intconlevel"
     linear-intargs-intvarargs-intreltype-int-intconlevel)
    :void (home :pointer) (a :pointer) (x :pointer) (r int-rel-type) (c :int)
    (icl int-con-level))
(defcfun*
    ("linear_intargs_intvarargs_intreltype_intvar_intconlevel"
     linear-intargs-intvarargs-intreltype-intvar-intconlevel)
    :void (home :pointer) (a :pointer) (x :pointer) (r int-rel-type) (y :pointer)
    (icl int-con-level))
(defcfun*
    ("linear_intargs_intvarargs_intreltype_int_boolvar_intconlevel"
     linear-intargs-intvarargs-intreltype-int-boolvar-intconlevel)
    :void (home :pointer) (a :pointer) (x :pointer) (r int-rel-type) (c :int)
    (b :pointer) (icl int-con-level))
(defcfun*
    ("linear_intargs_intvarargs_intreltype_intvar_boolvar_intconlevel"
     linear-intargs-intvarargs-intreltype-intvar-boolvar-intconlevel)
    :void (home :pointer) (a :pointer) (x :pointer) (r int-rel-type) (y :pointer)
    (b :pointer) (icl int-con-level))
(defcfun*
    ("linear_boolvarargs_intreltype_int_intconlevel"
     linear-boolvarargs-intreltype-int-intconlevel)
    :void (home :pointer) (x :pointer) (r int-rel-type) (c :int)
    (icl int-con-level))
(defcfun*
    ("linear_boolvarargs_intreltype_intvar_intconlevel"
     linear-boolvarargs-intreltype-intvar-intconlevel)
    :void (home :pointer) (x :pointer) (r int-rel-type) (y :pointer)
    (icl int-con-level))
(defcfun*
    ("cumulatives_intvarargs_intvarargs_intvarargs_intvarargs_intvarargs_intargs_bool_intconlevel"
     cumulatives-intvarargs-intvarargs-intvarargs-intvarargs-intvarargs-intargs-bool-intconlevel)
    :void (home :pointer) (machine :pointer) (start :pointer) (duration :pointer)
    (end :pointer) (height :pointer) (limit :pointer) (at_most :boolean)
    (icl int-con-level))
(defcfun*
    ("cumulatives_intargs_intvarargs_intvarargs_intvarargs_intvarargs_intargs_bool_intconlevel"
     cumulatives-intargs-intvarargs-intvarargs-intvarargs-intvarargs-intargs-bool-intconlevel)
    :void (home :pointer) (machine :pointer) (start :pointer) (duration :pointer)
    (end :pointer) (height :pointer) (limit :pointer) (at_most :boolean)
    (icl int-con-level))
(defcfun*
    ("cumulatives_intvarargs_intvarargs_intargs_intvarargs_intvarargs_intargs_bool_intconlevel"
     cumulatives-intvarargs-intvarargs-intargs-intvarargs-intvarargs-intargs-bool-intconlevel)
    :void (home :pointer) (machine :pointer) (start :pointer) (duration :pointer)
    (end :pointer) (height :pointer) (limit :pointer) (at_most :boolean)
    (icl int-con-level))
(defcfun*
    ("cumulatives_intargs_intvarargs_intargs_intvarargs_intvarargs_intargs_bool_intconlevel"
     cumulatives-intargs-intvarargs-intargs-intvarargs-intvarargs-intargs-bool-intconlevel)
    :void (home :pointer) (machine :pointer) (start :pointer) (duration :pointer)
    (end :pointer) (height :pointer) (limit :pointer) (at_most :boolean)
    (icl int-con-level))
(defcfun*
    ("cumulatives_intvarargs_intvarargs_intvarargs_intvarargs_intargs_intargs_bool_intconlevel"
     cumulatives-intvarargs-intvarargs-intvarargs-intvarargs-intargs-intargs-bool-intconlevel)
    :void (home :pointer) (machine :pointer) (start :pointer) (duration :pointer)
    (end :pointer) (height :pointer) (limit :pointer) (at_most :boolean)
    (icl int-con-level))
(defcfun*
    ("cumulatives_intargs_intvarargs_intvarargs_intvarargs_intargs_intargs_bool_intconlevel"
     cumulatives-intargs-intvarargs-intvarargs-intvarargs-intargs-intargs-bool-intconlevel)
    :void (home :pointer) (machine :pointer) (start :pointer) (duration :pointer)
    (end :pointer) (height :pointer) (limit :pointer) (at_most :boolean)
    (icl int-con-level))
(defcfun*
    ("cumulatives_intvarargs_intvarargs_intargs_intvarargs_intargs_intargs_bool_intconlevel"
     cumulatives-intvarargs-intvarargs-intargs-intvarargs-intargs-intargs-bool-intconlevel)
    :void (home :pointer) (machine :pointer) (start :pointer) (duration :pointer)
    (end :pointer) (height :pointer) (limit :pointer) (at_most :boolean)
    (icl int-con-level))
(defcfun*
    ("cumulatives_intargs_intvarargs_intargs_intvarargs_intargs_intargs_bool_intconlevel"
     cumulatives-intargs-intvarargs-intargs-intvarargs-intargs-intargs-bool-intconlevel)
    :void (home :pointer) (machine :pointer) (start :pointer) (duration :pointer)
    (end :pointer) (height :pointer) (limit :pointer) (at_most :boolean)
    (icl int-con-level))
(defcfun* ("eq_intvar_intvar_intconlevel" eq-intvar-intvar-intconlevel) :void
  (home :pointer) (x0 :pointer) (x1 :pointer) (icl int-con-level))
(defcfun* ("eq_intvar_int_intconlevel" eq-intvar-int-intconlevel) :void
  (home :pointer) (x :pointer) (n :int) (icl int-con-level))
(defcfun*
    ("eq_intvar_intvar_boolvar_intconlevel" eq-intvar-intvar-boolvar-intconlevel)
    :void (home :pointer) (x0 :pointer) (x1 :pointer) (b :pointer)
    (icl int-con-level))
(defcfun*
    ("eq_intvar_int_boolvar_intconlevel" eq-intvar-int-boolvar-intconlevel) :void
  (home :pointer) (x :pointer) (n :int) (b :pointer) (icl int-con-level))
(defcfun* ("eq_intvarargs_intconlevel" eq-intvarargs-intconlevel) :void
  (home :pointer) (x :pointer) (icl int-con-level))
(defcfun*
    ("count_intvarargs_int_intreltype_int_intconlevel"
     count-intvarargs-int-intreltype-int-intconlevel)
    :void (home :pointer) (x :pointer) (n :int) (r int-rel-type) (m :int)
    (icl int-con-level))
(defcfun*
    ("count_intvarargs_intvar_intreltype_int_intconlevel"
     count-intvarargs-intvar-intreltype-int-intconlevel)
    :void (home :pointer) (x :pointer) (y :pointer) (r int-rel-type) (m :int)
    (icl int-con-level))
(defcfun*
    ("count_intvarargs_int_intreltype_intvar_intconlevel"
     count-intvarargs-int-intreltype-intvar-intconlevel)
    :void (home :pointer) (x :pointer) (n :int) (r int-rel-type) (z :pointer)
    (icl int-con-level))
(defcfun*
    ("count_intvarargs_intvar_intreltype_intvar_intconlevel"
     count-intvarargs-intvar-intreltype-intvar-intconlevel)
    :void (home :pointer) (x :pointer) (y :pointer) (r int-rel-type) (z :pointer)
    (icl int-con-level))
;;;;;;;;;;;;;;;;;;;;???
;(defcfun*
;    ("gcc_intvarargs_intargs_int_int_int_int_int_intconlevel"
;     gcc-intvarargs-intargs-int-int-int-int-int-intconlevel)
;    :void (home :pointer) (x :pointer) (c :pointer) (m :int) (unspec_low :int)
;    (unspec_up :int) (min :int) (max :int) (icl int-con-level))
;(defcfun*
;    ("gcc_intvarargs_intargs_int_int_int_int_intconlevel"
;     gcc-intvarargs-intargs-int-int-int-int-intconlevel)
;    :void (home :pointer) (x :pointer) (c :pointer) (m :int) (unspec :int)
;    (min :int) (max :int) (icl int-con-level))
;(defcfun*
;    ("gcc_intvarargs_int_int_intconlevel" gcc-intvarargs-int-int-intconlevel)
;    :void (home :pointer) (x :pointer) (lb :int) (ub :int) (icl int-con-level))
;(defcfun* ("gcc_intvarargs_int_intconlevel" gcc-intvarargs-int-intconlevel)
;    :void (home :pointer) (x :pointer) (ub :int) (icl int-con-level))
;(defcfun*
;    ("gcc_intvarargs_intvarargs_int_int_intconlevel"
;     gcc-intvarargs-intvarargs-int-int-intconlevel)
;    :void (home :pointer) (x :pointer) (c :pointer) (min :int) (max :int)
;    (icl int-con-level))
;(defcfun*
;    ("gcc_intvarargs_intargs_intvarargs_int_int_int_bool_int_int_intconlevel"
;     gcc-intvarargs-intargs-intvarargs-int-int-int-bool-int-int-intconlevel)
;    :void (home :pointer) (x :pointer) (v :pointer) (c :pointer) (m :int)
;    (unspec_low :int) (unspec_up :int) (all :boolean) (min :int) (max :int)
;    (icl int-con-level))
;(defcfun*
;    ("gcc_intvarargs_intargs_intvarargs_int_int_bool_int_int_intconlevel"
;     gcc-intvarargs-intargs-intvarargs-int-int-bool-int-int-intconlevel)
;    :void (home :pointer) (x :pointer) (v :pointer) (c :pointer) (m :int)
;    (unspec :int) (all :boolean) (min :int) (max :int) (icl int-con-level))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Replacing Old gcc interface
(defcfun*
    ("count_intvarargs_intvarargs_intcontlevel"
     count-intvarargs-intvarargs-intconlevel)
    :void (home :pointer) (x :pointer) (c :pointer) (icl int-con-level))
;;;

(defcfun*
    ("channel_intvarargs_intvarargs_intconlevel"
     channel-intvarargs-intvarargs-intconlevel)
    :void (home :pointer) (x :pointer) (y :pointer) (icl int-con-level))
(defcfun* ("dom_intvar_int_int_intconlevel" dom-intvar-int-int-intconlevel)
    :void (home :pointer) (x :pointer) (l :int) (m :int) (icl int-con-level))
(defcfun*
    ("dom_intvarargs_int_int_intconlevel" dom-intvarargs-int-int-intconlevel)
    :void (home :pointer) (x :pointer) (l :int) (m :int) (icl int-con-level))
(defcfun* ("dom_intvar_intset_intconlevel" dom-intvar-intset-intconlevel) :void
  (home :pointer) (x :pointer) (s :pointer) (icl int-con-level))
(defcfun*
    ("dom_intvarargs_intset_intconlevel" dom-intvarargs-intset-intconlevel) :void
  (home :pointer) (x :pointer) (s :pointer) (icl int-con-level))
(defcfun*
    ("dom_intvar_int_int_boolvar_intconlevel"
     dom-intvar-int-int-boolvar-intconlevel)
    :void (home :pointer) (x :pointer) (l :int) (m :int) (b :pointer)
    (icl int-con-level))
(defcfun*
    ("dom_intvar_intset_boolvar_intconlevel"
     dom-intvar-intset-boolvar-intconlevel)
    :void (home :pointer) (x :pointer) (s :pointer) (b :pointer)
    (icl int-con-level))
(defcfun*
    ("bool_not_boolvar_boolvar_intconlevel" bool-not-boolvar-boolvar-intconlevel)
    :void (home :pointer) (b0 :pointer) (b1 :pointer) (icl int-con-level))
(defcfun*
    ("bool_eq_boolvar_boolvar_intconlevel" bool-eq-boolvar-boolvar-intconlevel)
    :void (home :pointer) (b0 :pointer) (b1 :pointer) (icl int-con-level))
(defcfun*
    ("bool_and_boolvar_boolvar_boolvar_intconlevel"
     bool-and-boolvar-boolvar-boolvar-intconlevel)
    :void (home :pointer) (b0 :pointer) (b1 :pointer) (b2 :pointer)
    (icl int-con-level))
(defcfun*
    ("bool_and_boolvar_boolvar_bool_intconlevel"
     bool-and-boolvar-boolvar-bool-intconlevel)
    :void (home :pointer) (b0 :pointer) (b1 :pointer) (b2 :boolean)
    (icl int-con-level))
(defcfun*
    ("bool_and_boolvarargs_boolvar_intconlevel"
     bool-and-boolvarargs-boolvar-intconlevel)
    :void (home :pointer) (b :pointer) (c :pointer) (icl int-con-level))
(defcfun*
    ("bool_and_boolvarargs_bool_intconlevel"
     bool-and-boolvarargs-bool-intconlevel)
    :void (home :pointer) (b :pointer) (c :boolean) (icl int-con-level))
(defcfun*
    ("bool_or_boolvar_boolvar_boolvar_intconlevel"
     bool-or-boolvar-boolvar-boolvar-intconlevel)
    :void (home :pointer) (b0 :pointer) (b1 :pointer) (b2 :pointer)
    (icl int-con-level))
(defcfun*
    ("bool_or_boolvar_boolvar_bool_intconlevel"
     bool-or-boolvar-boolvar-bool-intconlevel)
    :void (home :pointer) (b0 :pointer) (b1 :pointer) (b2 :boolean)
    (icl int-con-level))
(defcfun*
    ("bool_or_boolvarargs_boolvar_intconlevel"
     bool-or-boolvarargs-boolvar-intconlevel)
    :void (home :pointer) (b :pointer) (c :pointer) (icl int-con-level))
(defcfun*
    ("bool_or_boolvarargs_bool_intconlevel" bool-or-boolvarargs-bool-intconlevel)
    :void (home :pointer) (b :pointer) (c :boolean) (icl int-con-level))
(defcfun*
    ("bool_imp_boolvar_boolvar_boolvar_intconlevel"
     bool-imp-boolvar-boolvar-boolvar-intconlevel)
    :void (home :pointer) (b0 :pointer) (b1 :pointer) (b2 :pointer)
    (icl int-con-level))
(defcfun*
    ("bool_imp_boolvar_boolvar_bool_intconlevel"
     bool-imp-boolvar-boolvar-bool-intconlevel)
    :void (home :pointer) (b0 :pointer) (b1 :pointer) (b2 :boolean)
    (icl int-con-level))
(defcfun*
    ("bool_eqv_boolvar_boolvar_boolvar_intconlevel"
     bool-eqv-boolvar-boolvar-boolvar-intconlevel)
    :void (home :pointer) (b0 :pointer) (b1 :pointer) (b2 :pointer)
    (icl int-con-level))
(defcfun*
    ("bool_eqv_boolvar_boolvar_bool_intconlevel"
     bool-eqv-boolvar-boolvar-bool-intconlevel)
    :void (home :pointer) (b0 :pointer) (b1 :pointer) (b2 :boolean)
    (icl int-con-level))
(defcfun*
    ("bool_xor_boolvar_boolvar_boolvar_intconlevel"
     bool-xor-boolvar-boolvar-boolvar-intconlevel)
    :void (home :pointer) (b0 :pointer) (b1 :pointer) (b2 :pointer)
    (icl int-con-level))
(defcfun*
    ("bool_xor_boolvar_boolvar_bool_intconlevel"
     bool-xor-boolvar-boolvar-bool-intconlevel)
    :void (home :pointer) (b0 :pointer) (b1 :pointer) (b2 :boolean)
    (icl int-con-level))
(defcfun*
    ("sortedness_intvarargs_intvarargs_intconlevel"
     sortedness-intvarargs-intvarargs-intconlevel)
    :void (home :pointer) (x :pointer) (y :pointer) (icl int-con-level))
(defcfun*
    ("sortedness_intvarargs_intvarargs_intvarargs_intconlevel"
     sortedness-intvarargs-intvarargs-intvarargs-intconlevel)
    :void (home :pointer) (x :pointer) (y :pointer) (z :pointer)
    (icl int-con-level))
(defcfun* ("distinct_intvarargs_intconlevel" distinct-intvarargs-intconlevel)
    :void (home :pointer) (x :pointer) (icl int-con-level))
(defcfun*
    ("distinct_intargs_intvarargs_intconlevel"
     distinct-intargs-intvarargs-intconlevel)
    :void (home :pointer) (n :pointer) (x :pointer) (icl int-con-level))
(defcfun*
    ("element_intargs_intvar_intvar_intconlevel"
     element-intargs-intvar-intvar-intconlevel)
    :void (home :pointer) (n :pointer) (x0 :pointer) (x1 :pointer)
    (icl int-con-level))
(defcfun*
    ("element_intvarargs_intvar_intvar_intconlevel"
     element-intvarargs-intvar-intvar-intconlevel)
    :void (home :pointer) (x :pointer) (y0 :pointer) (y1 :pointer)
    (icl int-con-level))
(defcfun*
    ("rel_intvar_intreltype_intvar_intconlevel"
     rel-intvar-intreltype-intvar-intconlevel)
    :void (home :pointer) (x0 :pointer) (r int-rel-type) (x1 :pointer)
    (icl int-con-level))
(defcfun*
    ("rel_intvar_intreltype_int_intconlevel"
     rel-intvar-intreltype-int-intconlevel)
    :void (home :pointer) (x :pointer) (r int-rel-type) (c :int)
    (icl int-con-level))
(defcfun*
    ("rel_intvar_intreltype_intvar_boolvar_intconlevel"
     rel-intvar-intreltype-intvar-boolvar-intconlevel)
    :void (home :pointer) (x0 :pointer) (r int-rel-type) (x1 :pointer)
    (b :pointer) (icl int-con-level))
(defcfun*
    ("rel_intvar_intreltype_int_boolvar_intconlevel"
     rel-intvar-intreltype-int-boolvar-intconlevel)
    :void (home :pointer) (x :pointer) (r int-rel-type) (c :int) (b :pointer)
    (icl int-con-level))
(defcfun*
    ("rel_intvarargs_intreltype_intvarargs_intconlevel"
     rel-intvarargs-intreltype-intvarargs-intconlevel)
    :void (home :pointer) (x :pointer) (r int-rel-type) (y :pointer)
    (icl int-con-level))
(defcfun*
    ("min_intvar_intvar_intvar_intconlevel" min-intvar-intvar-intvar-intconlevel)
    :void (home :pointer) (x0 :pointer) (x1 :pointer) (x2 :pointer)
    (icl int-con-level))
(defcfun*
    ("min_intvarargs_intvar_intconlevel" min-intvarargs-intvar-intconlevel) :void
  (home :pointer) (x :pointer) (y :pointer) (icl int-con-level))
(defcfun*
    ("max_intvar_intvar_intvar_intconlevel" max-intvar-intvar-intvar-intconlevel)
    :void (home :pointer) (x0 :pointer) (x1 :pointer) (x2 :pointer)
    (icl int-con-level))
(defcfun*
    ("max_intvarargs_intvar_intconlevel" max-intvarargs-intvar-intconlevel) :void
  (home :pointer) (x :pointer) (y :pointer) (icl int-con-level))
(defcfun* ("abs_intvar_intvar_intconlevel" abs-intvar-intvar-intconlevel) :void
  (home :pointer) (x0 :pointer) (x1 :pointer) (icl int-con-level))
(defcfun*
    ("mult_intvar_intvar_intvar_intconlevel"
     mult-intvar-intvar-intvar-intconlevel)
    :void (home :pointer) (x0 :pointer) (x1 :pointer) (x2 :pointer)
    (icl int-con-level))

;;; Sets

(defcfun gec-fs-make :pointer
  "Make completely undetermined SetVar with empty
greatest lower and full least upper bound.."
  (space :pointer))

(defcfun gec-fs-make-const :pointer
  "Make constant SetVar of CARD with DOM (IntSet)."
  (space :pointer)
  (dom :pointer))

(defcfun gec-fs-make-bounds :pointer
  "Make SetVar with lower-bound and upper-bound.
The dom pointers are IntSets."
  (space :pointer)  
  (lower-dom :pointer)  
  (upper-dom :pointer))

(defcfun gec-fs-make-lower-bound :pointer
  "Make SetVar with lower-bound and empty upper-bound."
  (space :pointer)
  (lower-dom :pointer))

(defcfun gec-fs-make-upper-bound :pointer
  "Make SetVar with empty lower-bound and given upper-bound."
  (space :pointer)
  (upper-dom :pointer))

(defcfun gec-fs-glb-size :unsigned-int
  "Return number of elements in the greatest lower bound."
  (set :pointer))

(defcfun gec-fs-lub-size :unsigned-int
  "Return number of elements in the least upper bound."
  (set :pointer))

(defcfun gec-fs-unknown-size :unsigned-int
  "Return number of unknown elements (elements in lub but not in glb)."
  (set :pointer))

(defcfun gec-fs-card-min :unsigned-int
  "Return cardinality minimum."
  (set :pointer))

(defcfun gec-fs-card-max :unsigned-int
  "Return cardinality maximum."
  (set :pointer))

(defcfun gec-fs-lub-min :int
  "Return minimum element of least upper bound."
  (set :pointer))

(defcfun gec-fs-lub-max :int
  "Return maximum element of least upper bound."
  (set :pointer))

(defcfun gec-fs-glb-min :int
  "Return minimum element of greatest lower bound."
  (set :pointer))

(defcfun gec-fs-glb-max :int
  "Return maximum of greatest lower bound."
  (set :pointer))

(defcfun gec-fs-contains :boolean
  "Test whether i is in greatest lower bound."
  (set :pointer)
  (x :int))

(defcfun gec-fs-not-contains :boolean
  "Test whether i is not in the least upper bound."
  (set :pointer)
  (x :int))

(defcfun gec-fs-assigned :boolean
  "Test whether this variable is assigned."
  (set :pointer))

(defcfun gec-fs-variable :pointer
  "Return set variable implementation."
  (set :pointer))

;;

(defcfun* ("make_LubRanges" make-LubRanges) :pointer
  (v :pointer))

(defcfun* ("LubRanges_min" LubRanges-min) :int
  (r :pointer))

(defcfun* ("LubRanges_max" LubRanges-max) :int
  (r :pointer))

(defcfun* ("LubRanges_width" LubRanges-width) :unsigned-int
  (r :pointer))

(defcfun* ("LubRanges_valid" LubRanges-valid) :boolean
  (r :pointer))

(defcfun* ("LubRanges_next" LubRanges-next) :void
  (r :pointer))

(defcfun* ("delete_LubRanges" delete-LubRanges) :void
  (r :pointer))

;;

(defcfun* ("make_GlbRanges" make-GlbRanges) :pointer
  (v :pointer))

(defcfun* ("GlbRanges_min" GlbRanges-min) :int
  (r :pointer))

(defcfun* ("GlbRanges_max" GlbRanges-max) :int
  (r :pointer))

(defcfun* ("GlbRanges_width" GlbRanges-width) :unsigned-int
  (r :pointer))

(defcfun* ("GlbRanges_valid" GlbRanges-valid) :boolean
  (r :pointer))

(defcfun* ("GlbRanges_next" GlbRanges-next) :void
  (r :pointer))

(defcfun* ("delete_GlbRanges" delete-GlbRanges) :void
  (r :pointer))


;;

(defcfun*
    ("atmostOne_setvarargs_unsigned_int" atmostone-setvarargs-unsigned-int) :void
  (home :pointer) (x :pointer) (c :unsigned-int))
(defcfun* ("distinct_setvarargs_unsigned_int" distinct-setvarargs-unsigned-int)
    :void (home :pointer) (x :pointer) (c :unsigned-int))
(defcfun* ("min_setvar_intvar" min-setvar-intvar) :void (home :pointer)
          (s :pointer) (x :pointer))
(defcfun* ("max_setvar_intvar" max-setvar-intvar) :void (home :pointer)
          (s :pointer) (x :pointer))
(defcfun* ("match_setvar_intvarargs" match-setvar-intvarargs) :void
  (home :pointer) (s :pointer) (x :pointer))
(defcfun* ("channel_intvarargs_setvarargs" channel-intvarargs-setvarargs) :void
  (home :pointer) (x :pointer) (y :pointer))
(defcfun* ("cardinality_setvar_intvar" cardinality-setvar-intvar) :void
  (home :pointer) (s :pointer) (x :pointer))
(defcfun*
    ("weights_intargs_intargs_setvar_intvar"
     weights-intargs-intargs-setvar-intvar)
    :void (home :pointer) (elements :pointer) (weights :pointer) (x :pointer)
    (y :pointer))
(defcfun* ("convex_setvar" convex-setvar) :void (home :pointer) (x :pointer))
(defcfun* ("convexHull_setvar_setvar" convexhull-setvar-setvar) :void
  (home :pointer) (x :pointer) (y :pointer))
(defcfun*
    ("selectUnion_setvarargs_setvar_setvar" selectunion-setvarargs-setvar-setvar)
    :void (home :pointer) (x :pointer) (y :pointer) (z :pointer))
(defcfun*
    ("selectInter_setvarargs_setvar_setvar" selectinter-setvarargs-setvar-setvar)
    :void (home :pointer) (x :pointer) (y :pointer) (z :pointer))
(defcfun*
    ("selectInterIn_setvarargs_setvar_setvar_intset"
     selectinterin-setvarargs-setvar-setvar-intset)
    :void (home :pointer) (x :pointer) (y :pointer) (z :pointer)
    (universe :pointer))
(defcfun* ("selectDisjoint_setvarargs_setvar" selectdisjoint-setvarargs-setvar)
    :void (home :pointer) (x :pointer) (y :pointer))
(defcfun*
    ("selectSet_setvarargs_intvar_setvar" selectset-setvarargs-intvar-setvar)
    :void (home :pointer) (x :pointer) (y :pointer) (z :pointer))
(defcfun* ("rel_setvar_setreltype_setvar" rel-setvar-setreltype-setvar) :void
  (home :pointer) (x :pointer) (r set-rel-type) (y :pointer))
(defcfun*
    ("rel_setvar_setreltype_setvar_boolvar" rel-setvar-setreltype-setvar-boolvar)
    :void (home :pointer) (x :pointer) (r set-rel-type) (y :pointer) (b :pointer))
(defcfun* ("rel_setvar_setreltype_intvar" rel-setvar-setreltype-intvar) :void
  (home :pointer) (s :pointer) (r set-rel-type) (x :pointer))
(defcfun* ("rel_intvar_setreltype_setvar" rel-intvar-setreltype-setvar) :void
  (home :pointer) (x :pointer) (r set-rel-type) (s :pointer))
(defcfun*
    ("rel_setvar_setreltype_intvar_boolvar" rel-setvar-setreltype-intvar-boolvar)
    :void (home :pointer) (s :pointer) (r set-rel-type) (x :pointer) (b :pointer))
(defcfun*
    ("rel_intvar_setreltype_setvar_boolvar" rel-intvar-setreltype-setvar-boolvar)
    :void (home :pointer) (x :pointer) (r set-rel-type) (s :pointer) (b :pointer))
(defcfun* ("rel_setvar_intreltype_intvar" rel-setvar-intreltype-intvar) :void
  (home :pointer) (s :pointer) (r int-rel-type) (x :pointer))
(defcfun* ("rel_intvar_intreltype_setvar" rel-intvar-intreltype-setvar) :void
  (home :pointer) (x :pointer) (r int-rel-type) (s :pointer))
(defcfun*
    ("rel_setvar_setoptype_setvar_setreltype_setvar"
     rel-setvar-setoptype-setvar-setreltype-setvar)
    :void (home :pointer) (x :pointer) (op set-op-type) (y :pointer)
    (r set-rel-type) (z :pointer))
(defcfun* ("rel_setoptype_setvarargs_setvar" rel-setoptype-setvarargs-setvar)
    :void (home :pointer) (op set-op-type) (x :pointer) (y :pointer))
(defcfun* ("rel_setoptype_intvarargs_setvar" rel-setoptype-intvarargs-setvar)
    :void (home :pointer) (op set-op-type) (x :pointer) (y :pointer))
(defcfun*
    ("rel_intset_setoptype_setvar_setreltype_setvar"
     rel-intset-setoptype-setvar-setreltype-setvar)
    :void (home :pointer) (x :pointer) (op set-op-type) (y :pointer)
    (r set-rel-type) (z :pointer))
(defcfun*
    ("rel_setvar_setoptype_intset_setreltype_setvar"
     rel-setvar-setoptype-intset-setreltype-setvar)
    :void (home :pointer) (x :pointer) (op set-op-type) (y :pointer)
    (r set-rel-type) (z :pointer))
(defcfun*
    ("rel_setvar_setoptype_setvar_setreltype_intset"
     rel-setvar-setoptype-setvar-setreltype-intset)
    :void (home :pointer) (x :pointer) (op set-op-type) (y :pointer)
    (r set-rel-type) (z :pointer))
(defcfun*
    ("rel_intset_setoptype_intset_setreltype_setvar"
     rel-intset-setoptype-intset-setreltype-setvar)
    :void (home :pointer) (x :pointer) (op set-op-type) (y :pointer)
    (r set-rel-type) (z :pointer))
(defcfun*
    ("rel_intset_setoptype_setvar_setreltype_intset"
     rel-intset-setoptype-setvar-setreltype-intset)
    :void (home :pointer) (x :pointer) (op set-op-type) (y :pointer)
    (r set-rel-type) (z :pointer))
(defcfun*
    ("rel_setvar_setoptype_intset_setreltype_intset"
     rel-setvar-setoptype-intset-setreltype-intset)
    :void (home :pointer) (x :pointer) (op set-op-type) (y :pointer)
    (r set-rel-type) (z :pointer))
(defcfun* ("sequence_setvarargs" sequence-setvarargs) :void (home :pointer)
          (x :pointer))
(defcfun*
    ("sequentialUnion_setvarargs_setvar" sequentialunion-setvarargs-setvar) :void
  (home :pointer) (y :pointer) (x :pointer))
(defcfun* ("dom_setvar_setreltype_int" dom-setvar-setreltype-int) :void
  (home :pointer) (x :pointer) (r set-rel-type) (i :int))
(defcfun* ("dom_setvar_setreltype_int_int" dom-setvar-setreltype-int-int) :void
  (home :pointer) (x :pointer) (r set-rel-type) (i :int) (j :int))
(defcfun* ("dom_setvar_setreltype_intset" dom-setvar-setreltype-intset) :void
  (home :pointer) (x :pointer) (r set-rel-type) (s :pointer))
(defcfun*
    ("dom_setvar_setreltype_int_boolvar" dom-setvar-setreltype-int-boolvar) :void
  (home :pointer) (x :pointer) (r set-rel-type) (i :int) (b :pointer))
(defcfun*
    ("dom_setvar_setreltype_int_int_boolvar"
     dom-setvar-setreltype-int-int-boolvar)
    :void (home :pointer) (x :pointer) (r set-rel-type) (i :int) (j :int)
    (b :pointer))
(defcfun*
    ("dom_setvar_setreltype_intset_boolvar" dom-setvar-setreltype-intset-boolvar)
    :void (home :pointer) (x :pointer) (r set-rel-type) (s :pointer) (b :pointer))
(defcfun*
    ("cardinality_setvar_unsigned_int_unsigned_int"
     cardinality-setvar-unsigned-int-unsigned-int)
    :void (home :pointer) (x :pointer) (i :unsigned-int) (j :unsigned-int))
